# README #
* Version 1.0

### How do I get set up? ###

1. To run the test task must install Node.js and MongoDB.
2. To set depending on the terminal should run: "npm install"

### How run application? ###

1. To run the test task must typing in terminal: "node bin/www"
2. In browser enter address: "localhost:3000"

### How to use? ###

1. Subscriber fills a form and sends to server
2. Distributors of vacancies fills a form and send data to server. If data matched by criteria in subscribers then server sends emails to selected subscribers.

* The application can be run in the development and production modes, but if you do not use them then application is started in development mode.