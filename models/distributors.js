var mongoose = require("mongoose");

var distributorsSchema = mongoose.Schema({
    name: String,
    category : String,
    city    : String,
    salaryRange  : String,
    email   : {
        type: String,
        lowercase: true
    },
    companyName: String,
    vacancyTitle: String,
    description: String,
    publish: Boolean
});

module.exports = mongoose.model('Distributors', distributorsSchema);