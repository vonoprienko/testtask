var mongoose = require("mongoose");

var subscribersSchema = mongoose.Schema({
    name: String,
    categories : {
        type: []
    },
    city : String,
    salaryRange : String,
    email : {
        type: String,
        unique: true,
        lowercase: true
    },
    created: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Subscribers', subscribersSchema);