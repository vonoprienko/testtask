var express = require('express'),
    router = express.Router(),
    path = require('path'),
    Subscriptions = require('../models/subscriptions');
    Distributors = require('../models/distributors');

var settings = require(__dirname + '/../config/config'),
    sendEmail = settings.sendEmail;

/* GET home page */
router.get('/', function(req, res, next) {
    res.sendFile(path.join(__dirname + '/public/index.html'));
});

/* POST Subscribe user */
router.post('/subscribe', function(req, res, next) {
    var subscription = new Subscriptions(req.body);
    subscription.save()
        .then(function (data) { res.status(200).json(data) })
        .catch(function (err) {
            if (err.code === 11000) {
                res.status(500).json({message: "This email is already registered"});
            } else {
                res.status(500).json(err);
            }
        });
});

/* POST Distribute vacancy */
router.post('/distribute', function(req, res, next) {
    var distributors = new Distributors(req.body);
    distributors.save()
        .then(function (data) {
            Subscriptions.find({
                city: distributors.city,
                salaryRange: distributors.salaryRange,
                categories: distributors.category
            }, function (err, subscriptions) {
                if (err) { return res.status(500).json({ message: err}) }
                if (subscriptions.length > 0) {
                    subscriptions.forEach(function (subscriber) {
                        var content = distributors.description,
                            to = subscriber.email,
                            from = distributors.email,
                            title = distributors.vacancyTitle + ' [' + distributors.city + '] ' + ' [' + distributors.salaryRange + '] ' + ' [' + distributors.companyName + '] ';
                        sendEmail(from, to, title, content);
                    });
                    return res.status(200).json({message: 'Server is sending messages'});
                } else { return res.status(200).json({ message: 'Subscribers not found' }) }
            });
        })
        .catch(function (err) { res.status(500).json(err) });
});

/* GET All vacancies */
router.get('/jobs', function(req, res, next) {
    Distributors.find({}, function (err, distributors) {
        if (err) { return res.status(500).json({ message: err}) }
        return res.status(200).json(distributors);
    })
});

/* GET All subscriptions */
router.get('/subscriptions', function(req, res, next) {
    Subscriptions.find({}, function (err, subscriptions) {
        if (err) { return res.status(500).json({ message: err}) }
        return res.status(200).json(subscriptions);
    })
});

module.exports = router;