exports.config = {
    getEnv: function () {
        switch (process.env.NODE_ENV) {
            case 'dev':
                return {
                    port: 3000,
                    protocol: 'http://',
                    domain: "localhost",
                    mongodb: "mongodb://localhost:27017/testTask",
                    mailgun: {
                        key: 'key-cc5f4eb3ba23938ff5126ed828bb325f',
                        domain: 'sandbox4cd7db4c8830475bb6ce9f24a83af592.mailgun.org',
                        login: 'postmaster@sandbox4cd7db4c8830475bb6ce9f24a83af592.mailgun.org',
                        password: '1q2w3e'
                    }
                };
            case 'prod':
                return {
                    port: 443,
                    protocol: 'https://',
                    domain: "noname.com",
                    mongodb: "mongodb://localhost:27017/testTask",
                    mailgun: {
                        key: 'key-cc5f4eb3ba23938ff5126ed828bb325f',
                        domain: 'sandbox4cd7db4c8830475bb6ce9f24a83af592.mailgun.org',
                        login: 'postmaster@sandbox4cd7db4c8830475bb6ce9f24a83af592.mailgun.org',
                        password: '1q2w3e'
                    }
                };
            default:
                return {
                    port: 3000,
                    protocol: 'http://',
                    domain: "dev-noname.com",
                    mongodb: "mongodb://localhost:27017/testTask",
                    mailgun: {
                        key: 'key-cc5f4eb3ba23938ff5126ed828bb325f',
                        domain: 'sandbox4cd7db4c8830475bb6ce9f24a83af592.mailgun.org',
                        login: 'postmaster@sandbox4cd7db4c8830475bb6ce9f24a83af592.mailgun.org',
                        password: '1q2w3e'
                    }
                };
        }
    }
};

var mailgun = require('mailgun-js')({
        apiKey: exports.config.getEnv().mailgun.key,
        domain: exports.config.getEnv().mailgun.domain
});

/**
 * Emails delivery function
 * @param from {string} - sender
 * @param to {string} - email of recipient
 * @param title {string} - email title
 * @param content {string} - email text or html
 */
exports.sendEmail = function (from, to, title, content) {
    if (to) {
        var data = {
            from: from,
            to: to,
            subject: title,
            html: content
        };
        mailgun.messages().send(data, function (error, body) {
            if (error) next(error);
        });
    }
};