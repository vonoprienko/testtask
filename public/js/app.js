tinymce.init({
    selector: '#mytextarea',
    menubar: false,
    height: '200',
    toolbar1: "bold italic underline strikethrough searchreplace bullist numlist undo redo alignleft aligncenter alignright alignjustify outdent indent"
});

var vacancy_form = $("#vacancy_form"),
    subscription_form = $("#subscription_form"),
    popup_form = $("#popup_form"),
    popup = $(".popup"),
    popup_cancel = $(".popup .cancel"),
    backdrop = $(".backdrop"),
    selectors = $(".selectors li:not(:last-child)"),
    popupSelector = $("#popup_form .selector"),
    requestParams = {
        'categories': []
    };
var http = new XMLHttpRequest();

vacancy_form.submit(function (event) {
    event.preventDefault();
    requestParams = {
        name: 'vacancy',
        category: $('#vacancy_form li p')[0].textContent,
        city: $('#vacancy_form li p')[1].textContent,
        salaryRange: $('#vacancy_form li p')[2].textContent,
        email: this.email.value
    };
    if (requestParams.category !== 'Select category...' && requestParams.city !== 'Select city...' && requestParams.salaryRange !== 'Salary' && requestParams.email) {
        showPopup();
    }
});

subscription_form.submit(function (event) {
    event.preventDefault();
    var url = "/subscribe";
    requestParams['name'] = 'subscribe';
    requestParams['city'] = $('#subscription_form li p')[1].textContent;
    requestParams['salaryRange'] = $('#subscription_form li p')[2].textContent;
    requestParams['email'] = this.email.value;

    http.open("POST", url, true);
    http.setRequestHeader("Content-type", "application/json");
    http.onreadystatechange = function () {
        if (http.readyState == 4 && http.status == 200) {
            alert('Thank you for subscribing');
        } else if (http.readyState == 4 && http.status == 500) {
            alert(JSON.parse(http.responseText).message);
        }
    };

    if (requestParams.categories !== 'Select category...' && requestParams.city !== 'Select city...' && requestParams.salaryRange !== 'Salary' && requestParams.email) {
        http.send(JSON.stringify(requestParams));
        clearDatas();
    }
});

popup_form.submit(function (event) {
    event.preventDefault();
    var url = '/distribute';
    requestParams['vacancyTitle'] = this.vacancy_title.value;
    requestParams['companyName'] = this.company_name.value;
    requestParams['publish'] = this.publish.checked;
    requestParams['salaryRange'] = $('#popup_form .selector p')[0].textContent;
    requestParams['description'] = tinymce.activeEditor.getContent();


    http.open("POST", url, true);
    http.setRequestHeader("Content-type", "application/json");
    http.onreadystatechange = function () {
        if (http.readyState == 4 && http.status == 200) {
            alert('Thank you for vacancy');
        } else if (http.readyState == 4 && http.status == 500) {
            console.log(http)
        }
    };

    if (requestParams['description'] && requestParams['companyName'] && requestParams['vacancyTitle'] && requestParams['salaryRange']) {
        http.send(JSON.stringify(requestParams));
        hidePopup();
        clearDatas();
    }
});

popup_cancel.click(function () {
    event.preventDefault();
    hidePopup();
});

selectors.click(function () {
    var arr,
        options = $('.options')[0],
        li = '',
        selector = this,
        countCheckboxes = 0;

    switch (selector.getAttribute("data-id")) {
        case 'select_1':
            arr = ['IT', 'Sales', 'Healthcare', 'Accounting', 'Logistics'];
            break;
        case 'select_2':
            arr = ['New York', 'California', 'Illinois', 'Texas', 'Pennsylvania'];
            break;
        case 'select_3':
            arr = ['up to 200 000', '201 000 - 400 000', '401 000 - 600 000'];
    }

    for (var i = 0; i < arr.length; i++) {
        if (selector.getAttribute("data-selector") === 'three_categories') {
            if (requestParams['categories'].indexOf(arr[i]) > -1) {
                li += '<li><input type="checkbox" value="' + arr[i] + '" checked="true"><p>' + arr[i] + '</p></li>';
                countCheckboxes++;
            } else {
                li += '<li><input type="checkbox" value="' + arr[i] + '"><p>' + arr[i] + '</p></li>';
            }
        } else {
            li += '<li>' + arr[i] + '</li>'
        }
    }

    var optionsHTML = '<ul class="options">' + li + '</ul>';

    $('.open').removeClass('open');
    if (!selector.children[3]) {
        selector.innerHTML += optionsHTML;
        $(selector).addClass('open');
    }
    if (options) {
        options.outerHTML = '';
    }

    if (countCheckboxes >= 3) {
        for (var i = 0; i < $('.options input:not(:checked)').length; i++) {
            $('.options input:not(:checked)')[i].disabled = true;
        }
    } else {
        for (var i = 0; i < $('.options input:not(:checked)').length; i++) {
            $('.options input:not(:checked)')[i].disabled = false;
        }
    }
    $('.options li').click(function () {
        if ($('.options li input').length) {
            event.stopPropagation();
            if (this.children[0].disabled) {
                return false;
            }
            if (this.children[0].checked) {
                this.children[0].checked = false;
                requestParams['categories'].splice(requestParams['categories'].indexOf(this.children[1].textContent), 1);
                countCheckboxes--;
            } else {
                this.children[0].checked = true;
                requestParams['categories'].push(this.children[1].textContent);
                countCheckboxes++;
            }
            if (countCheckboxes === 3) {
                for (var i = 0; i < $('.options input:not(:checked)').length; i++) {
                    $('.options input:not(:checked)')[i].disabled = true;
                }
            } else {
                for (var i = 0; i < $('.options input:not(:checked)').length; i++) {
                    $('.options input:not(:checked)')[i].disabled = false;
                }
            }
            if (countCheckboxes === 0) {
                selector.children[1].textContent = 'Select up to three categories...';
            } else {
                selector.children[1].textContent = requestParams['categories'].join();
            }
        } else {
            selector.children[1].innerText = this.innerText;
        }
    });
});

popupSelector.click(function () {
    var arr = ['up to 200 000', '201 000 - 400 000', '401 000 - 600 000'],
        options = $('.options')[0],
        li = '',
        selector = this;

    for (var i = 0; i < arr.length; i++) {

        li += '<li>' + arr[i] + '</li>'
    }

    var optionsHTML = '<ul class="options">' + li + '</ul>';

    $('.open').removeClass('open');
    if (!selector.children[3]) {
        selector.innerHTML += optionsHTML;
        $(selector).addClass('open');
    }
    if (options) {
        options.outerHTML = '';
    }

    $('.options li').click(function () {
        selector.children[1].innerText = this.innerText;
    });
});

function showPopup() {
    popup.css({'display': 'block'});
    backdrop.css({'display': 'block'});
}

function hidePopup() {
    popup.css({'display': ''});
    backdrop.css({'display': ''});
}

function clearDatas() {
    $('#vacancy_form li p')[0].textContent = 'Select category...';
    $('#vacancy_form li p')[1].textContent = 'Select city...';
    $('#vacancy_form li p')[2].textContent = 'Salary';
    $('#subscription_form li p')[0].textContent = 'Select up to three categories...';
    $('#subscription_form li p')[1].textContent = 'Select city...';
    $('#subscription_form li p')[2].textContent = 'Salary';
    $('#popup_form .selector p')[0].innerHTML = 'Salary range <sup>*</sup>';
    $('#subscription_form')[0].reset();
    $('#vacancy_form')[0].reset();
    $('#popup_form')[0].reset();
    requestParams = {'categories': []};
    countCheckboxes = 0;
}